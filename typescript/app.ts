(()=>{

    const sumar = (a: number, b: number): number => a + b;

    const nombre = (): string => 'Hola Pedro';

    const obtenerSalario = ():Promise<string> => {
        return new Promise( (resolve, reject) => {
            resolve('Pedro');
        });
    }

    obtenerSalario().then( a => console.log( a.toUpperCase() ) )

})();