(function() {

    function getEdad() {
        return 100 + 100 + 300;
    }

    const nombre = 'Pedro';
    const apellido = 'Gomez';
    const edad = 23;

    // const salida = nombre + apellido + edad;
    // const salida = nombre + " " + apellido + " ( " + edad + " )";
    //  Pedro Gomez (Edad: 23)

    const salida = `
        ${ nombre }
        ${ apellido }
        ( ${ getEdad() } )`;

    console.log(salida);

})();


